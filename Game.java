import java.util.Scanner;

public class Game {
    public static void main(String[] args) {
        marubatsu();
    }

    public static void marubatsu() {

        // 3x3の盤をintの2次元配列で作成

        int[][] board = new int[3][3];
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                board[i][j] = 0;
            }
        }

        while (true) {

            // プレイヤーの丸を置く場所を入力。形式は1,1など。それ以外の形式は再入力を促す。すでに丸かバツが置かれている場所には置けない。

            Scanner scanner = new Scanner(System.in);
            while (true) {
                System.out.print("Enter x,y to place your move (e.g. 1,1): ");
                String input = scanner.nextLine();
                String[] parts = input.split(",");
                if (parts.length != 2) {
                    System.out.println("Invalid input. Please enter in format x,y");
                    continue;
                }

                int x, y;
                try {
                    x = Integer.parseInt(parts[0]);
                    y = Integer.parseInt(parts[1]);
                } catch (NumberFormatException e) {
                    System.out.println("Invalid input. Please enter numbers");
                    continue;
                }

                if (x < 0 || x >= 3 || y < 0 || y >= 3) {
                    System.out.println("Out of bounds. Please enter numbers between 0-2");
                    continue;
                }

                if (board[x][y] != 0) {
                    System.out.println("Position already taken. Please try again.");
                    continue;
                }

                board[x][y] = 1;
                break;
            }

            // コンピュータがバツを置く場所をランダムに決定。すでに丸かバツが置かれている場所には置けない

            int cpuX;
            int cpuY;
            do {
                cpuX = (int) (Math.random() * 3);
                cpuY = (int) (Math.random() * 3);
            } while (board[cpuX][cpuY] != 0);

            board[cpuX][cpuY] = 2; // 2 for computer's move

            // 盤を表示。丸をO、バツをXで表示
            for (int i = 0; i < 3; i++) {
                for (int j = 0; j < 3; j++) {
                    if (board[i][j] == 1) {
                        System.out.print("O ");
                    } else if (board[i][j] == 2) {
                        System.out.print("X ");
                    } else {
                        System.out.print("- ");
                    }
                }
                System.out.println();
            }

            // 丸かバツが、縦、横、斜めに3つ並べば、並んだ方の勝ち。勝った方の丸かバツを表示

            // Check for 3 in a row
            for (int i = 0; i < 3; i++) {
                if (board[i][0] == board[i][1] && board[i][1] == board[i][2] && board[i][0] != 0) {
                    // Horizontal win
                    if (board[i][0] == 1) {
                        System.out.println("You win horizontally!");
                    } else {
                        System.out.println("Computer wins horizontally!");
                    }
                    return;
                }
                if (board[0][i] == board[1][i] && board[1][i] == board[2][i] && board[0][i] != 0) {
                    // Vertical win
                    if (board[0][i] == 1) {
                        System.out.println("You win vertically!");
                    } else {
                        System.out.println("Computer wins vertically!");
                    }
                    return;
                }
            }

            if (board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[0][0] != 0) {
                // Diagonal \ win
                if (board[0][0] == 1) {
                    System.out.println("You win diagonally!");
                } else {
                    System.out.println("Computer wins diagonally!");
                }
                return;
            }

            if (board[0][2] == board[1][1] && board[1][1] == board[2][0] && board[0][2] != 0) {
                // Diagonal / win
                if (board[0][2] == 1) {
                    System.out.println("You win diagonally!");
                } else {
                    System.out.println("Computer wins diagonally!");
                }
                return;
            }

        }

    }
}
